import Action from './Action';
import Product from '../repository/models/Product';
import getProducts from '../repository/HttpRequestConfig';

export const SHOW_LOADING = 'show loading';
export const RECEIVE_PRODUCTS = 'receive products';
export const CATCH_ERROR = 'catch error';
export const SELECT_PRODUCT = 'select product';
export const PURCHASE = 'purchase';
export const REMOVE_PRODUCT = 'remove product';
const createAction = <T>(type: string, payload: T): Action<T> => ({
  type,
  payload,
});

const showLoading = (isLoading: boolean) =>
  createAction(SHOW_LOADING, isLoading);

const receiveProducts = (products: [Product]) =>
  createAction(RECEIVE_PRODUCTS, products);

const catchError = (error: string) => createAction(CATCH_ERROR, error);

export const selectProduct = (product: Product) =>
  createAction(SELECT_PRODUCT, product);

export const purchase = () => createAction(PURCHASE, undefined);

export const removeProduct = (product: Product) =>
  createAction(REMOVE_PRODUCT, product);

export const requestProducts = (onError: (err: string) => void) => {
  return (dispatch: any) => {
    dispatch(showLoading(true));
    getProducts({
      onSuccess: (products) => {
        dispatch(receiveProducts(products));
      },
      onError: (error) => {
        dispatch(catchError(error));
        onError(error);
      },
    });
  };
};
