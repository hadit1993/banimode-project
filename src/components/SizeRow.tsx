/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {View, Text, StyleSheet, TouchableWithoutFeedback} from 'react-native';
import ProductSize from '../repository/models/ProductSize';
import {toPersianNumber} from './numberConverter';

interface sizeInput {
  size: ProductSize;
  index: number;
  selectedIndex: number;
  onSelect: (index: number) => void;
}

const SizeRow = (props: sizeInput) => {
  const hasNumber = (input: string) => {
    return /\d/.test(input);
  };
  return (
    <TouchableWithoutFeedback onPress={() => props.onSelect(props.index)}>
      <Text
        style={{
          ...styles.text,
          marginRight: props.index === 0 ? 0 : 10,
          borderColor:
            props.selectedIndex === props.index ? '#1ac977' : '#d6d5d5',
          color: props.selectedIndex === props.index ? '#1ac977' : 'black',
        }}>
        {hasNumber(props.size.name)
          ? toPersianNumber(props.size.name)
          : props.size.name}
      </Text>
    </TouchableWithoutFeedback>
  );
};

export default SizeRow;

const styles = StyleSheet.create({
  text: {
    textAlign: 'center',
    textAlignVertical: 'center',
    width: 60,
    height: 60,
    borderWidth: 1,
    borderRadius: 10,

    fontFamily: 'IRANSansMobile_Medium',
    fontSize: 16,
  },
});
