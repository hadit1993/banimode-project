export const toPersianNumber = (number: string) => {
  const farsiDigits = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];

  return number.replace(/\d/g, (x) => farsiDigits[x]);
};

export const formatPrice = (price: number) => {
  const formattedPrice = price
    .toString()
    .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');

  return toPersianNumber(formattedPrice);
};
