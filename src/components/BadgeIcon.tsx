import React from 'react';
import {View, Image, StyleSheet, Text, ImageSourcePropType} from 'react-native';

const BadgeIcon = (props: {source: ImageSourcePropType; badgeText: string}) => {
  return (
    <View style={styles.item}>
      <Image resizeMode="stretch" source={props.source} style={styles.icon} />

      <View style={styles.badge_container}>
        {props.badgeText !== '۰' && (
          <Text style={styles.badge}>{props.badgeText}</Text>
        )}
      </View>
    </View>
  );
};

export default BadgeIcon;

const styles = StyleSheet.create({
  item: {width: 40, height: 40, justifyContent: 'center'},
  icon: {width: 30, height: 30},
  badge_container: {
    position: 'absolute',
    width: 40,
    height: 40,
    alignItems: 'flex-end',
  },
  badge: {
    minWidth: 20,
    height: 20,
    fontFamily: 'IRANSansMobile',
    paddingLeft: 5,
    paddingRight: 5,
    textAlign: 'center',
    fontSize: 12,
    backgroundColor: '#1ac977',
    color: 'white',
    borderRadius: 10,
  },
});
