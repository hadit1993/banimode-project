import React from 'react';
import {View, Image, StyleSheet, Text, ImageSourcePropType} from 'react-native';

const Icon = (props: {source: ImageSourcePropType}) => {
  return (
    <View style={styles.item}>
      <Image resizeMode="stretch" source={props.source} style={styles.icon} />
    </View>
  );
};

const styles = StyleSheet.create({
  item: {width: 40, height: 40, justifyContent: 'center'},
  icon: {width: 30, height: 30},
});

export default Icon;
