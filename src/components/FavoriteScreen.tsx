/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import {useEffect, useState, useRef} from 'react';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  FlatList,
  Image,
  TouchableWithoutFeedback,
  ToastAndroid,
} from 'react-native';
import {connect} from 'react-redux';
import RBSheet from 'react-native-raw-bottom-sheet';
import {HomeState} from '../reducer/HomeReducer';
import {
  requestProducts,
  selectProduct,
  purchase,
  removeProduct,
} from '../action/HomeAction';
import {Spinner, Item} from 'native-base';
import ProductItem from './ProductItem';
import help from '../assets/images/help.png';
import Product from '../repository/models/Product';
import SizeRow from './SizeRow';
import PurchaseBSContent from './PurchaseBSContent';
import ConfirmBSContent from './ConfirmBSContent';
import empty from '../assets/images/empty.png';

const FavoriteScreen = (props: MainProps) => {
  useEffect(() => {
    props.getProducts((err) => {
      ToastAndroid.show(err, ToastAndroid.LONG);
    });
  }, []);
  const refPurchaseRBSheet = useRef<RBSheet>();
  const refConfirmRBSheet = useRef<RBSheet>();

  const isProductsAvailable = () => {
    return !props.isLoading && props.products && props.products.length > 0;
  };

  const isEmpty = () => {
    return (
      props.error === '' &&
      !props.isLoading &&
      props.products &&
      props.products.length === 0
    );
  };

  return (
    <View style={styles.container}>
      {props.isLoading && <Spinner color="#1ac977" />}
      {isProductsAvailable() && (
        <FlatList
          style={styles.grid}
          data={props.products}
          renderItem={({item}) => (
            <ProductItem
              product={item}
              onPurchase={() => {
                props.select(item);
                refPurchaseRBSheet.current?.open();
              }}
              onRemove={(prod) => {
                props.remove(prod);
              }}
            />
          )}
          keyExtractor={(item) => `${item.id_product}`}
          numColumns={2}
          columnWrapperStyle={styles.columnWrapper}
          contentContainerStyle={styles.flatListContainer}
        />
      )}

      {isEmpty() && (
        <View style={styles.emptyContainer}>
          <Image source={empty} style={styles.emptyImage} />

          <Text style={styles.emptyText}>علاقه مندی ای وجود ندارد</Text>
        </View>
      )}

      <RBSheet
        ref={refPurchaseRBSheet}
        height={340}
        openDuration={500}
        customStyles={{
          container: styles.rbSheet,
        }}>
        <PurchaseBSContent
          product={props.selectedProduct}
          onPurchase={() => {
            props.purchaseProduct();
            refPurchaseRBSheet.current?.close();
            refConfirmRBSheet.current?.open();
          }}
        />
      </RBSheet>

      <RBSheet
        ref={refConfirmRBSheet}
        height={420}
        openDuration={500}
        customStyles={{
          container: styles.rbSheet,
        }}>
        <ConfirmBSContent
          onConfirm={() => {
            refConfirmRBSheet.current?.close();
          }}
        />
      </RBSheet>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyContainer: {alignItems: 'center'},
  emptyText: {
    marginTop: 20,
    fontFamily: 'IRANSansMobile_Bold',
    fontSize: 18,
  },
  columnWrapper: {justifyContent: 'space-between'},
  emptyImage: {width: 130, height: 130},
  flatListContainer: {padding: 16},
  grid: {width: '100%', backgroundColor: 'transparent', height: '100%'},
  rbSheet: {borderTopLeftRadius: 30, borderTopRightRadius: 30},
});

interface Navigation {
  navigation: any;
  route?: any;
}

const mapStateToProps = (state: HomeState): HomeState => ({
  ...state,
});

const mapDispatchToProps = {
  getProducts: (onError: (err: string) => void) => requestProducts(onError),
  select: (product: Product) => selectProduct(product),
  purchaseProduct: () => purchase(),
  remove: (product: Product) => removeProduct(product),
};

type MainProps = HomeState & typeof mapDispatchToProps & Navigation;

export default connect(mapStateToProps, mapDispatchToProps)(FavoriteScreen);
