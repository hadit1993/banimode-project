import React, {useState} from 'react';
import {
  View,
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import help from '../assets/images/help.png';
import SizeRow from './SizeRow';
import Product from '../repository/models/Product';
import purchaseWhite from '../assets/images/purchase_white.png';
import confirmPurchase from '../assets/images/confirm_purchase.png';

interface ConfirmBSInput {
  onConfirm: () => void;
}

const ConfirmBSContent = (props: ConfirmBSInput) => {
  return (
    <View style={styles.root}>
      <View style={styles.header} />

      <Image source={confirmPurchase} style={styles.image} />

      <Text style={styles.goodChoice}>انتخابت عالی بود!</Text>

      <Text style={styles.addedToBasket}>
        کالای مورد نظر با موفقیت به سبد خرید افزوده شد
      </Text>

      <TouchableOpacity
        onPress={props.onConfirm}
        style={styles.continueContainer}>
        <Text style={styles.continue}>ادامه دادن خرید</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={props.onConfirm} style={styles.seeContainer}>
        <Text style={styles.see}>مشاهده سبد خرید</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ConfirmBSContent;

const styles = StyleSheet.create({
  root: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  header: {
    borderRadius: 2,
    width: 60,
    height: 5,
    backgroundColor: '#d6d5d5',
    marginTop: 20,
  },
  image: {width: 120, height: 120, marginTop: 20},
  goodChoice: {
    marginTop: 20,
    color: '#1ac977',
    fontFamily: 'IRANSansMobile_Bold',
    fontSize: 18,
  },
  addedToBasket: {
    marginTop: 15,
    color: 'black',
    fontFamily: 'IRANSansMobile_Bold',
    fontSize: 14,
  },
  continueContainer: {width: '100%', marginTop: 15},
  continue: {
    borderWidth: 1,
    marginLeft: 16,
    marginRight: 16,
    height: 60,
    borderRadius: 10,
    borderColor: '#d6d5d5',
    fontFamily: 'IRANSansMobile_Medium',
    color: 'black',
    fontSize: 16,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  seeContainer: {width: '100%', marginBottom: 30, marginTop: 10},
  see: {
    backgroundColor: '#1ac977',
    marginLeft: 16,
    marginRight: 16,
    height: 60,
    borderRadius: 10,
    textAlign: 'center',
    textAlignVertical: 'center',
    fontFamily: 'IRANSansMobile_Medium',
    color: 'white',
    fontSize: 16,
  },
});
