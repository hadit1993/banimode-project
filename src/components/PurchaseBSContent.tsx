import React, {useState} from 'react';
import {
  View,
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import help from '../assets/images/help.png';
import SizeRow from './SizeRow';
import Product from '../repository/models/Product';
import purchaseWhite from '../assets/images/purchase_white.png';

interface PurchaseBSInput {
  product?: Product;
  onPurchase: () => void;
}

const PurchaseBSContent = (props: PurchaseBSInput) => {
  const [selectedIndex, setSelectedIndex] = useState<number>(0);
  return (
    <View style={styles.root}>
      <View style={styles.header} />

      <Text style={styles.selectSize}>
        لطفا سایز مناسب خود را انتخاب نمایید
      </Text>

      <View style={styles.sizeGuideContainer}>
        <Text style={styles.sizeGuide}>راهنمای سایز</Text>

        <Image style={styles.icon} source={help} />
      </View>

      {props.product && (
        <FlatList
          inverted={true}
          showsHorizontalScrollIndicator={false}
          style={styles.flatList}
          data={props.product.size}
          keyExtractor={(item) => item.id_size.toString()}
          horizontal={true}
          contentContainerStyle={styles.rowContainer}
          renderItem={({item, index}) => (
            <SizeRow
              size={item}
              index={index}
              selectedIndex={selectedIndex}
              onSelect={(ind) => setSelectedIndex(ind)}
            />
          )}
        />
      )}
      <TouchableOpacity onPress={props.onPurchase} style={styles.addBasketBtn}>
        <View style={styles.addBasketContainer}>
          <Text style={styles.addBasket}>افزودن به سبد خرید</Text>
          <Image style={styles.icon} source={purchaseWhite} />
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default PurchaseBSContent;

const styles = StyleSheet.create({
  root: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  header: {
    borderRadius: 2,
    width: 60,
    height: 5,
    backgroundColor: '#d6d5d5',
    marginTop: 20,
  },
  selectSize: {
    marginTop: 25,
    fontFamily: 'IRANSansMobile_Bold',
    fontSize: 15,
  },
  sizeGuideContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '70%',
    borderRadius: 10,
    height: 50,
    backgroundColor: '#f0f0f0',
    marginTop: 15,
  },
  sizeGuide: {
    color: '#1ac977',
    fontFamily: 'IRANSansMobile_Bold',
    fontSize: 16,
    marginRight: 10,
  },
  icon: {width: 22, height: 22},
  flatList: {marginTop: 25},
  rowContainer: {paddingLeft: 16, paddingRight: 16},
  addBasketBtn: {width: '100%', marginBottom: 30},
  addBasketContainer: {
    backgroundColor: '#1ac977',
    marginLeft: 16,
    marginRight: 16,
    height: 60,
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  addBasket: {
    fontFamily: 'IRANSansMobile_Medium',
    color: 'white',
    fontSize: 16,
    marginRight: 15,
  },
});
