/* eslint-disable react/react-in-jsx-scope */
import React from 'react';
import {View, StyleSheet} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {connect} from 'react-redux';
import FavoriteScreen from './FavoriteScreen';
import favorite from '../assets/images/favourite.png';
import home from '../assets/images/home.png';
import profile from '../assets/images/profile.png';
import search from '../assets/images/search.png';
import basket from '../assets/images/basket.png';
import Icon from './Icon';
import BadgeIcon from './BadgeIcon';
import {HomeState} from '../reducer/HomeReducer';
import {toPersianNumber} from './numberConverter';

interface BottomTabState {
  purchaseCount: number;
}

const BottomTabNavigator = (props: BottomTabState) => {
  const Tab = createBottomTabNavigator();

  return (
    <Tab.Navigator
      initialRouteName="Articles"
      tabBar={() => {
        return (
          <View style={styles.container}>
            <Icon source={profile} />
            <Icon source={favorite} />
            <BadgeIcon
              source={basket}
              badgeText={`${toPersianNumber(`${props.purchaseCount}`)}`}
            />
            <Icon source={search} />
            <Icon source={home} />
          </View>
        );
      }}>
      <Tab.Screen name="FavoriteScreen" component={FavoriteScreen} />
    </Tab.Navigator>
  );
};

const mapStateToProps = (state: HomeState): BottomTabState => ({
  purchaseCount: state.purchaseCount,
});

export default connect(mapStateToProps)(BottomTabNavigator);

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    backgroundColor: 'white',
    height: 60,
    flexDirection: 'row',
    paddingLeft: 16,
    paddingRight: 16,
    alignItems: 'center',
    borderTopWidth: 0.5,
    borderTopColor: '#d8d7d5',
  },
  item: {width: 40, height: 40, justifyContent: 'center'},
  icon: {width: 30, height: 30},
  badge_container: {
    position: 'absolute',
    width: 40,
    height: 40,
    alignItems: 'flex-end',
  },
  badge: {
    minWidth: 20,
    height: 20,
    fontFamily: 'IRANSansMobile',
    paddingLeft: 5,
    paddingRight: 5,
    textAlign: 'center',
    fontSize: 12,
    backgroundColor: '#1ac977',
    color: 'white',
    borderRadius: 10,
  },
});
