/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Image,
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import Product from '../repository/models/Product';
import {toPersianNumber, formatPrice} from './numberConverter';
import close from '../assets/images/close.png';
import purchase from '../assets/images/purchase.png';

interface inputProps {
  product: Product;
  onRemove: (product: Product) => void;
  onPurchase: (product: Product) => void;
}

const itemWidth = Dimensions.get('window').width / 2 - 32;

const ProductItem = (props: inputProps) => {
  return (
    <View
      style={{width: itemWidth, marginTop: 10, backgroundColor: 'transparent'}}>
      <View style={styles.imageContainer}>
        <Image
          source={{uri: props.product.images.large_default[0]}}
          style={styles.image}
        />

        <View style={styles.shadow}>
          <View
            style={{
              ...styles.discountContainer,
              justifyContent: props.product.product_specific_price
                ? 'space-between'
                : 'flex-end',
            }}>
            {props.product.product_specific_price && (
              <Text style={styles.discount}>
                {`٪ ${toPersianNumber(
                  `${props.product.product_specific_price.discount_percent}`,
                )}`}
              </Text>
            )}

            <TouchableOpacity
              onPress={() => props.onRemove(props.product)}
              style={{marginRight: 10}}>
              <Image source={close} style={styles.icon} />
            </TouchableOpacity>
          </View>

          <TouchableOpacity
            onPress={() => props.onPurchase(props.product)}
            style={{marginBottom: 10, marginLeft: 10}}>
            <View style={styles.whiteRound}>
              <Image source={purchase} style={styles.icon} />
            </View>
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.footer}>
        <Text numberOfLines={1} style={styles.manufacturer}>
          {props.product.product_manufacturer_en_name}
        </Text>

        <Text numberOfLines={1} style={styles.name}>
          {props.product.product_name}
        </Text>

        <View style={styles.priceContainer}>
          <Text style={styles.currentPrice}>
            {props.product.product_specific_price
              ? formatPrice(props.product.product_specific_price.specific_price)
              : formatPrice(props.product.product_price)}{' '}
            تومان
          </Text>
          {props.product.product_specific_price && (
            <Text style={styles.formerPrice}>
              {formatPrice(props.product.product_price)} تومان
            </Text>
          )}
        </View>
      </View>
    </View>
  );
};

export default ProductItem;

const styles = StyleSheet.create({
  imageContainer: {
    width: '100%',
    height: itemWidth * 1.3,

    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {width: '60%', height: '60%', borderRadius: 10},
  shadow: {
    backgroundColor: 'rgba(0, 0, 0, 0.03)',
    borderRadius: 10,
    width: '100%',
    height: '100%',
    position: 'absolute',
    justifyContent: 'space-between',
  },
  discountContainer: {
    width: '100%',
    flexDirection: 'row',
    marginTop: 10,
  },
  discount: {
    height: 20,
    width: 35,
    textAlign: 'center',
    textAlignVertical: 'center',
    backgroundColor: '#f16422',
    color: 'white',
    borderBottomRightRadius: 10,
    borderTopRightRadius: 10,
    fontSize: 12,
    fontFamily: 'IRANSansMobile',
  },
  icon: {width: 25, height: 25},
  whiteRound: {
    width: 25,
    height: 25,
    backgroundColor: 'white',
    borderRadius: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {width: '100%', alignItems: 'flex-end'},
  manufacturer: {fontSize: 12, marginTop: 10, fontFamily: 'Gilroy-Bold'},
  name: {
    fontSize: 11,
    marginTop: 10,
    fontFamily: 'IRANSansMobile_Medium',
  },
  priceContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  currentPrice: {
    marginTop: 10,
    fontFamily: 'IRANSansMobile_Medium',
    fontSize: 11,
    color: '#1ac977',
  },
  formerPrice: {
    marginTop: 10,
    fontFamily: 'IRANSansMobile_Medium',
    fontSize: 11,
    color: '#6f7377',
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
    marginLeft: 5,
  },
});
