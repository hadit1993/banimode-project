import Action from '../action/Action';
import Product from '../repository/models/Product';
import {
  SHOW_LOADING,
  CATCH_ERROR,
  SELECT_PRODUCT,
  PURCHASE,
  REMOVE_PRODUCT,
} from '../action/HomeAction';

export interface HomeState {
  isLoading: boolean;
  products: [Product];
  error: string;
  selectedProduct?: Product;
  purchaseCount: number;
}

const HomeReducer = (
  currentState: HomeState = {
    isLoading: false,
    products: [],
    error: '',
    purchaseCount: 0,
  },
  action: Action<any>,
): HomeState => {
  switch (action.type) {
    case SHOW_LOADING:
      return {...currentState, isLoading: action.payload};
    case CATCH_ERROR:
      return {...currentState, error: action.payload, isLoading: false};

    case SELECT_PRODUCT:
      return {...currentState, selectedProduct: action.payload};

    case PURCHASE:
      return {...currentState, purchaseCount: currentState.purchaseCount + 1};

    case REMOVE_PRODUCT: {
      var products: [Product] = [...currentState.products];
      console.log('products befor remove', products);
      const product: Product = action.payload as Product;
      const index = products.indexOf(product);
      console.log('index', index);
      products.splice(index, 1);
      console.log('products after remove', products);
      return {...currentState, products};
    }

    default:
      return {
        ...currentState,
        products: action.payload,
        isLoading: false,
        error: '',
      };
  }
};

export default HomeReducer;
