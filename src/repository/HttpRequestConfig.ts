import BaseResponse from './models/BaseResponse';
import Product from './models/Product';

const apiUrl = 'https://mobapi.banimode.com/api/v1/wishlist';
const authToken =
  'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImNiYTEyOTZkYjU3ZjgzYjkwZjBmMThmYTA0NzQwOWFjYzYzZjZmNTA2ZjU3MjdhYTAyMDM5ZjZiMWNkMTRmYjNjYTI1MzJjMGMxODZkODljIn0.eyJhdWQiOiIxIiwianRpIjoiY2JhMTI5NmRiNTdmODNiOTBmMGYxOGZhMDQ3NDA5YWNjNjNmNmY1MDZmNTcyN2FhMDIwMzlmNmIxY2QxNGZiM2NhMjUzMmMwYzE4NmQ4OWMiLCJpYXQiOjE1OTI2NjI2ODcsIm5iZiI6MTU5MjY2MjY4NywiZXhwIjoxNjI0MTk4Njg3LCJzdWIiOiIyNTA3MjMiLCJzY29wZXMiOnsicGxhY2Utb3JkZXJzIjoxfX0.CePXluOWctM12tDdvKrWRj0irK4hbcMTOa7CsSUKElmSNznvvMQd445HdkucmKThHMWrdIdg82540FW70fHTZNgxoF7jcPkNntY9kMKe-fzZl88m699XpcMqOfetWqDNQs_PFozCze6qYRT-PooINaUCjDOuCqg1Pw3s4aWIxN24jY7IqIRiwCR3l7wOK5G_3qh_le7d5zoV7RRxSB_z7zo5WaRtn4Ylw4pdGTGUZ4MwHHEwh0Fu_iT_9welIR1pVjaUEXLeXDyQNN9TbNOimJv6gbAcTDqXG5IQviaGzAF9q95PuHfGg_Ui9UwmU67FEOQB7jYwpp8nzEGBoAUf13fpwAtNpx9CeiC1AHdasQhlBYXydM_RSv_0PfDDIYFkFvK42G84OyoEHaYMHKUrb79H3rBZY0CW1CZP82bAe9uPlzVpZeYvmrpGrTOv08KLI2mJPtEDOM_ZoRAQUUpipse0DhHza2HnF2GCSSwFfOnTuVsVtHjfJ792MIenOPvTA-uD-kBXHJl_7FkC76UFCBRgR7w4OD7A7tP0IwsxaLF-4ay2seJbmiHKfkh019sgch0QZjPwPtg9I0KP_ZZMwkDdBQM06oO00efUxezfK0Yih6lMOGSttUGYNrCBmDbq2kS-onqFmrGBDG8L174TRefWEq0ZBr3_bEQOldJGUbo';

const parseResponse = (response: Response): Promise<BaseResponse> => {
  const json = response.json() as Promise<BaseResponse>;
  console.log('json response: ', json);

  return json;
};

interface ResponseCallback {
  onSuccess: (result: [Product]) => void;
  onError: (error: string) => void;
}

const request = (): Promise<Response> => {
  const init: RequestInit = {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: authToken,
    },
  };

  return fetch(apiUrl, init);
};

const getFavoriteProducts = async (callback: ResponseCallback) => {
  const publicError = 'خطای ناشناخته. لطفا دوباره تلاش کنید';

  try {
    const response = await request();
    try {
      const parsedResponse = await parseResponse(response);
      if (
        parsedResponse.status_code === 200 &&
        parsedResponse.status === 'success'
      ) {
        callback.onSuccess(parsedResponse.data.data);
      } else {
        callback.onError(parsedResponse.message);
      }
    } catch (parseError) {
      console.log('parse error: ', parseError);
      callback.onError(publicError);
    }
  } catch (reqError) {
    console.log('request error: ', reqError);
    callback.onError(publicError);
  }
};

export default getFavoriteProducts;
