export default class ProductSize {
  name!: string;
  slug!: string;
  id_size!: number;
  position!: number;
  reference!: number;
  id_product_attribute!: number;
  active!: number;
  quantity!: number;
}
