import Product from './Product';

export default class BaseResponse {
  status!: string;
  status_code!: number;
  message!: string;
  data!: ResponseData;
}

class ResponseData {
  data!: [Product];
}
