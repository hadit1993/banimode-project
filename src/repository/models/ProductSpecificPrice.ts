export default class ProductSpecificPrice {
  specific_price!: number;
  discount_amount!: number;
  discount_percent!: number;
}
