export default class ProductImage {
  cart_default!: [string];
  small_default!: [string];
  medium_default!: [string];
  home_default!: [string];
  large_default!: [string];
  thickbox_default!: [string];
}
