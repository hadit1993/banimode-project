import ProductImage from './ProductImage';
import ProductSpecificPrice from './ProductSpecificPrice';
import ProductSize from './ProductSize';

export default class Product {
  id_product!: number;
  id_product_attribute!: number;
  product_name!: string;
  product_price!: number;
  product_manufacturer_name!: string;
  product_manufacturer_en_name!: string;
  product_manufacturer_id!: number;
  product_manufacturer_slug!: string;
  product_specific_price!: ProductSpecificPrice;
  images!: ProductImage;
  size!: [ProductSize];
}
