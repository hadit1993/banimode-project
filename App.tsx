import React from 'react';

import ArticleDetail from './src/components/ArticleDetail';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import BottomTab from './src/components/BottomTabNavigator';
import {Tab} from 'native-base';

const App = () => {
  const Stack = createStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="bottomTab"
        screenOptions={{
          headerStyle: {backgroundColor: 'white'},
          headerTitleStyle: {
            color: 'black',
            fontFamily: 'IRANSansMobile_Bold',
            fontSize: 15,
          },
          headerTintColor: 'black',
          headerTitleAlign: 'center',
        }}>
        <Stack.Screen
          name="bottomTab"
          component={BottomTab}
          options={{title: 'لیست علاقه مندی ها'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

// const styles = StyleSheet.create({

// });

export default App;
