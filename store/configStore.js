import {createStore, compose, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import HomeReducer from '../src/reducer/HomeReducer';

const middleWare = applyMiddleware (thunk);
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const configureStore = () => {
  return createStore (HomeReducer, composeEnhancers (middleWare));
};

export default configureStore;
